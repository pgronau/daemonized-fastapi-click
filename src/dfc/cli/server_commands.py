from time import sleep

import click
from dfc.api.fastapi_server import stop_daemon, start_daemon


@click.group()
def server():
    pass


@server.command()
def start():
    """
    Starts a daemonized server.
    """
    print("Starting daemon.")
    start_daemon("127.0.0.1", 5000)
    print("Daemon started ")
    sleep(1)


@server.command()
def stop():
    """
    Signals the server to stop.
    """
    stop_daemon()
