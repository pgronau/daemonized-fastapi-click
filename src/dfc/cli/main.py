import click as click

from dfc.cli import server_commands


@click.group()
def cli():
    pass


cli.add_command(server_commands.start)
cli.add_command(server_commands.stop)


if __name__ == "__main__":
    # cli(["--help"])
    cli()
    # cli(["stop"])
