import atexit
import os
from pathlib import Path
from time import sleep

import uvicorn as uvicorn
from daemon import DaemonContext
from daemon import pidfile
from fastapi import FastAPI

CONFIG_DIR = f"{str(Path.home())}/.config/daemon-fastapi-click/"
PID_FILE = f"{CONFIG_DIR}server.pid"

app = FastAPI(debug=True)


@app.get("/check_health")
async def health_check():
    return {"status": "UP"}


def start_server(host: str, port_: int):
    uvicorn.run(
        "dfc.api.fastapi_server:app",
        host=host,
        port=port_,
        log_level="info",
    )


def cleanup():
    os.remove(PID_FILE)


def start_daemon(ip, port):
    """
    Starts a server daemon.

    Reads config file from ~/.config/daemon-fastapi-click/server_cfg.yml.
    Creates PID file at ~/.config/daemon-fastapi-click/server.pid for non root usage.
    Returns
    -------

    """
    Path(CONFIG_DIR).mkdir(parents=True, exist_ok=True)
    if os.path.isfile(PID_FILE):
        print("PID file exists: Daemon already running?")
    pid_lock_file = pidfile.PIDLockFile(PID_FILE)
    context = DaemonContext(pidfile=pid_lock_file)
    with context:
        sleep(1)
        print("hi")
        atexit.register(cleanup)
        start_server(ip, port)
        sleep(1)
    sleep(1)


def stop_daemon():
    """
    Sends SIGTERM to the running daemon.

    """
    with open(PID_FILE, "r") as f:
        pid = int(f.read())
    os.kill(pid, 15)
