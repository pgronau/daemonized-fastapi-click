This is an example on how to integrate `click`, `fastapi` and `python-daemon` to create a local fastapi app that can be run without root permission.

It consists of two parts with each brining two important concepts.
```
daemonized-api
├── api (fastapi, python-daemon)
│   ├── fastapi
│   └── daemon
└── cli (click)
    ├── core command line tool
    └── server commands

```

Install and run with:
```python
pip3 install -r requirements.txt
cd src/
python3 -m dfc.cli.main start
python3 -m dfc.cli.main stop
```
